package com.schneider.preprocessor;

import com.schneider.preprocessor.model.DCIDocument;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.xml.stream.XMLStreamException;

public class Main {

    private static final Logger LOG = Logger.getLogger("[Main]");

    static List<DCIDocument> docList = new ArrayList<DCIDocument>();

    public static void main(String[] args) {
        //PropertyConfigurator.configure("log4j.properties");

        args = new String[2];

        args[0] = "D:\\JIRA\\WMS-81\\dest";
        args[1] = "D:\\JIRA\\WMS-81\\DCIPreprocessor.props";

        if ((args != null) && (args.length == 2)) {

            String xmlDestFolderPath = args[0];
            String propsFilePath = args[1];

            LOG.info("Properties file path " + propsFilePath);

            File propsFile = new File(propsFilePath);
            if (propsFile.exists()) {
                try {
                    HashMap<String, String[]> preprocessorProps = fetchPreprocessorProperties(new FileInputStream(propsFile));

                    String xmlDocsForAutonomyFilePath = preprocessorProps.get(DCIPreprocessorConstants.DOCS_FOR_AUTONOM)[0];

                    File xmlDocsForAutonomyFile = new File(xmlDocsForAutonomyFilePath);
                    if (xmlDocsForAutonomyFile.exists()) {
                        LOG.info("Parsing XML " + xmlDocsForAutonomyFile);
                        docList = XMLParser.parseMainXML(xmlDocsForAutonomyFile);
                        LOG.info("Number of valid <doc> fetched: " + docList.size());
                        if(docList.size()>0){
                            HashMap<String, List> docTypeMap = XMLParser.parseDocTypeXML(preprocessorProps);
                            if (docTypeMap.size() > 0){
                                LOG.info("Writing XML...");
                                DCIXMLWriter.writeXML(xmlDestFolderPath, docList, preprocessorProps, docTypeMap);
                                LOG.info("XML writing successfully completed.");

                            }
                        }
                    } else {
                        LOG.debug("XML file does not exist at " + xmlDocsForAutonomyFilePath);
                    }
                } catch (FileNotFoundException e) {
                    LOG.error("FileNotFoundException occurred: ", e);
                } catch (IOException e) {
                    LOG.error("IO exception occurred: ", e);
                } catch (XMLStreamException e) {
                    LOG.error("ParseError exception occurred: ", e);
                } finally {
                    //docList = null;
                }
            }else {
                LOG.info("Can not find properties file at the path " + propsFilePath);
            }

        } else {
            LOG.debug("Usage: OnePopXmlPreprocessor <XML file destination path> <Properties file path> ");
        }
    }

    /*public static HashMap<String, String> fetchPreprocessorProperties(FileInputStream preprocessorPropStream) throws IOException {
        HashMap<String, String> preprocessorPropsMap = new HashMap<String, String>();
        Properties preprocessorProps = new Properties();
        preprocessorProps.load(preprocessorPropStream);
        Set<Object> targetSet = preprocessorProps.keySet();
        for(Object target : targetSet){
            String val = preprocessorProps.getProperty((String)target);
            System.out.println(target.toString()+"===Info==="+ val+"\n");
            preprocessorPropsMap.put((String)target, val);
        }
        return preprocessorPropsMap;
    }*/

    public static HashMap<String, String[]> fetchPreprocessorProperties(FileInputStream preprocessorPropStream) throws IOException {
        HashMap<String, String[]> preprocessorPropsMap = new HashMap<String, String[]>();
        Properties preprocessorProps = new Properties();
        preprocessorProps.load(preprocessorPropStream);
        Set<Object> targetSet = preprocessorProps.keySet();
        for(Object target : targetSet){
            String[] langArr = null;
            String lang = preprocessorProps.getProperty((String)target);
            if(lang.contains(DCIPreprocessorConstants.COMMA)){
                langArr = lang.split(DCIPreprocessorConstants.COMMA);
            }else{
                langArr = new String[1];
                langArr[0] = lang;
            }
            preprocessorPropsMap.put((String)target, langArr);
        }
        return preprocessorPropsMap;
    }
}
