package com.schneider.preprocessor;

import com.schneider.preprocessor.model.DCIDocument;
import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by SESA380090 on 2016-06-15.
 */
public class XMLParser {

    private static final Logger LOG = Logger.getLogger("[XMLParser]");

    public static List<DCIDocument> parseMainXML(File xmlSrcFile) throws FileNotFoundException, XMLStreamException {
        List<DCIDocument> docList = new ArrayList<DCIDocument>();

        FileInputStream xmlInputStream = new FileInputStream(xmlSrcFile);
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        inputFactory.setProperty(XMLInputFactory.IS_COALESCING , true);
        XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(xmlInputStream);

        LOG.debug("Fetching <doc> details...");

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if ((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.DOC))) {
                DCIDocument doc = new DCIDocument();
                parseDocNode(xmlEventReader,xmlEvent, doc);
                if((doc.getId()!=null)&&(doc.getRangeId()!=null)&&(doc.getTarget()!=null)){
                    if(doc.getRangeId().contains(DCIPreprocessorConstants.VALUE_SEP)){
                        LOG.debug("[XML INFO] Multiple rangeId for <doc>: id=" + doc.getId());
                    }
                    if(doc.getDescription()==null){
                        LOG.debug("[XML WARNING] Description missing for <doc>: id=" + doc.getId());
                    }
                    docList.add(doc);
                }
                else{
                    LOG.debug("[XML ERROR] Mandatory field(s) missing for <doc>: id=" + doc.getId() + " | rangeId=" + doc.getRangeId() + " | commercialRef=" + doc.getCommercialRef()+  " | target=" + doc.getTarget());
                }
            }
        }
        xmlEventReader.close();
        return docList;
    }
    
    public static void parseDocNode(XMLEventReader xmlEventReader, XMLEvent xmlEvent, DCIDocument doc) throws XMLStreamException{
        while(xmlEventReader.hasNext()){
            xmlEvent = xmlEventReader.nextEvent();
            if((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.ARR))){
                Attribute attribute = xmlEvent.asStartElement().getAttributeByName(new QName(DCIPreprocessorConstants.NAME));
                if(attribute!=null){
                    String attrVal = attribute.getValue();
                    if(attrVal.equals(DCIPreprocessorConstants.TARGETS)){
                        parseAttrTargets(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.RANGEID)){
                        parseRangeId(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.ID)){
                        //parseDocId(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.FILENAME)){
                        parseFileName(xmlEventReader, xmlEvent, doc);
                    }
                }
            }else if((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                Attribute attribute = xmlEvent.asStartElement().getAttributeByName(new QName(DCIPreprocessorConstants.NAME));
                if(attribute!=null){
                    String attrVal = attribute.getValue();
                    if(attrVal.equals(DCIPreprocessorConstants.COMM_REF)){
                        //String commercialRef = getTagValue(xmlEventReader, xmlEvent, doc);
                        //doc.setCommercialRef(commercialRef);
                    }else if(attrVal.equals(DCIPreprocessorConstants.ID)){
                        //String docID = xmlEventReader.getElementText().toString();
                        String docID = getTagValue(xmlEventReader, xmlEvent, doc);
                        doc.setId(docID);
                    }else if(attrVal.equals(DCIPreprocessorConstants.NAME)) {
                        //String EDMSname = xmlEventReader.getElementText().toString();
                        String EDMSname = getTagValue(xmlEventReader, xmlEvent, doc);
                        doc.setEdmsName(EDMSname);
                    }else if(attrVal.contains(DCIPreprocessorConstants.TITLE)) {
                        //String title = xmlEventReader.getElementText().toString();
                        //String title = getTagValue(xmlEventReader, xmlEvent, doc);
                        parseTitle(xmlEventReader, xmlEvent, doc, attrVal);
                        LOG.info("TITLE >> " + doc.getTitle());
                    }else if(attrVal.contains(DCIPreprocessorConstants.DESCRIPTION)) {
                        //String description = xmlEventReader.getElementText().toString();
                        parseDesc(xmlEventReader, xmlEvent, doc, attrVal);
                        LOG.info("DESCRIPTION >> " + doc.getDescription());
                    }else if(attrVal.contains(DCIPreprocessorConstants.DOC_TYPE_ID)) {
                        //String docTypeID = xmlEventReader.getElementText().toString();
                        String docTypeID = getTagValue(xmlEventReader, xmlEvent, doc);
                        LOG.info("DOC_TYPE_ID >> " + docTypeID);
                        doc.setDocTypeID(docTypeID);
                    }
                }
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.DOC))){
                break;
            }

        }
    }

    static void parseRangeId(XMLEventReader xmlEventReader,XMLEvent xmlEvent, DCIDocument doc)throws XMLStreamException {
        while (xmlEventReader.hasNext()) {
            xmlEvent = xmlEventReader.nextEvent();
            if ((xmlEvent.isStartElement())&& (xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                String rangeId = xmlEventReader.nextEvent().asCharacters().getData();
                String rangeIdFromDoc = doc.getRangeId();
                if(rangeIdFromDoc == null){
                    doc.setRangeId(rangeId);
                }else{
                    rangeId = rangeIdFromDoc + DCIPreprocessorConstants.VALUE_SEP + rangeId;
                    doc.setRangeId(rangeId);
                }
                LOG.info("Range id >> "+doc.getRangeId());

            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.ARR))){
                break;
            }
        }

    }

    static void parseFileName(XMLEventReader xmlEventReader,XMLEvent xmlEvent, DCIDocument doc)throws XMLStreamException {
        List <String> fileNames = new ArrayList<String>();
        while (xmlEventReader.hasNext()) {
            xmlEvent = xmlEventReader.nextEvent();
            if ((xmlEvent.isStartElement())&& (xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                String fileName = xmlEventReader.nextEvent().asCharacters().getData();
                fileNames.add(fileName);
                LOG.info("fileName >> " + fileName);
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.ARR))){
                break;
            }
        }

    }

    public static HashMap<String, List> parseDocTypeXML(HashMap<String, String[]> preprocessorProps) throws FileNotFoundException, XMLStreamException {
        HashMap<String, List> docTypeMap = new HashMap<String, List>();
        String xmlDocTypeFilePath = preprocessorProps.get(DCIPreprocessorConstants.DOC_TYPE_SERVICE)[0];

        File xmlDocTypeFile = new File(xmlDocTypeFilePath);
        if (xmlDocTypeFile.exists()) {
            LOG.info("Parsing DocType XML " + xmlDocTypeFile);

            FileInputStream xmlInputStream = new FileInputStream(xmlDocTypeFile);
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            inputFactory.setProperty(XMLInputFactory.IS_COALESCING , true);
            XMLEventReader xmlEventReader = inputFactory.createXMLEventReader(xmlInputStream);

            LOG.debug("Fetching <doc> details...");

            while (xmlEventReader.hasNext()) {

                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if ((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.DOC))) {
                    parseDocNode(xmlEventReader,xmlEvent, docTypeMap);
                }

            }
        } else {
            LOG.debug("XML file does not exist at " + xmlDocTypeFile);
        }

        return docTypeMap;
    }

    public static void parseDocNode(XMLEventReader xmlEventReader, XMLEvent xmlEvent, HashMap<String, List> docTypeMap) throws XMLStreamException{
        List<String> docList = new ArrayList<String>();
        String docTypeID = null;
        while(xmlEventReader.hasNext()){
            xmlEvent = xmlEventReader.nextEvent();
            if((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.ARR))){
                Attribute attribute = xmlEvent.asStartElement().getAttributeByName(new QName(DCIPreprocessorConstants.NAME));
                if(attribute!=null){
                    String attrVal = attribute.getValue();
                    if(attrVal.equals(DCIPreprocessorConstants.TARGETS)){
                        //parseAttrTargets(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.RANGEID)){
                        //parseRangeId(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.ID)){
                        //parseDocId(xmlEventReader, xmlEvent, doc);
                    }else if(attrVal.equals(DCIPreprocessorConstants.FILENAME)){
                        //parseFileName(xmlEventReader, xmlEvent, doc);
                    }
                }
            }else if((xmlEvent.isStartElement())&&(xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                Attribute attribute = xmlEvent.asStartElement().getAttributeByName(new QName(DCIPreprocessorConstants.NAME));
                if(attribute!=null){

                    String docTypeName = null;
                    String attrVal = attribute.getValue();
                    if(attrVal.equals(DCIPreprocessorConstants.COMM_REF)){
                        //String commercialRef = getTagValue(xmlEventReader, xmlEvent, doc);
                        //doc.setCommercialRef(commercialRef);
                    }else if(attrVal.equals(DCIPreprocessorConstants.ID)){
                        docTypeID = xmlEventReader.getElementText().toString();
                        LOG.info("DOC TYPE ID >> " + docTypeID);
                    }else if(attrVal.contains(DCIPreprocessorConstants.NAME_T)) {
                        docTypeName = xmlEventReader.getElementText().toString();
                        docList.add(docTypeName);
                        //LOG.info("DOC TYPE NAME >> " + docTypeName);
                    }
                }
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.DOC))){
                docTypeMap.put(docTypeID,docList);
                break;
            }

        }

    }

    static void parseAttrTargets(XMLEventReader xmlEventReader,XMLEvent xmlEvent, DCIDocument doc)throws XMLStreamException {
        while (xmlEventReader.hasNext()) {
            xmlEvent = xmlEventReader.nextEvent();
            if ((xmlEvent.isStartElement())&& (xmlEvent.asStartElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                String target = xmlEventReader.nextEvent().asCharacters().getData();
                String targetFromDoc = doc.getTarget();
                if(targetFromDoc == null){
                    doc.setTarget(target);
                }else{
                    target = targetFromDoc + DCIPreprocessorConstants.VALUE_SEP + target;
                    doc.setTarget(target);
                }

            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.ARR))){
                break;
            }
        }

    }

    static String getTagValue(XMLEventReader xmlEventReader,XMLEvent xmlEvent, DCIDocument doc)throws XMLStreamException {
        String tagValue = DCIPreprocessorConstants.BLANK;
        while (xmlEventReader.hasNext()) {
            xmlEvent = xmlEventReader.nextEvent();
            if(xmlEvent.isCharacters()){
                tagValue = xmlEvent.asCharacters().getData();
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                break;
            }
        }
        return tagValue;
    }

    static void parseDesc(XMLEventReader xmlEventReader, XMLEvent xmlEvent, DCIDocument doc, String attrVal) throws XMLStreamException{
        while(xmlEventReader.hasNext()){
            xmlEvent = xmlEventReader.nextEvent();
            if(xmlEvent.isCharacters()){
                String desc = xmlEvent.asCharacters().getData();
                String descAttrVal = attrVal + DCIPreprocessorConstants.ATTR_NAME_SEP + desc;
                if(doc.getDescription() == null){
                    doc.setDescription(descAttrVal);
                }else{
                    String prevDesc = doc.getDescription();
                    descAttrVal = prevDesc + DCIPreprocessorConstants.VALUE_SEP + descAttrVal;
                    doc.setDescription(descAttrVal);
                }
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                break;
            }
        }
    }

    static void parseTitle(XMLEventReader xmlEventReader, XMLEvent xmlEvent, DCIDocument doc, String attrVal) throws XMLStreamException{
        while(xmlEventReader.hasNext()){
            xmlEvent = xmlEventReader.nextEvent();
            if(xmlEvent.isCharacters()){
                String title = xmlEvent.asCharacters().getData();
                String titleAttrVal = attrVal + DCIPreprocessorConstants.ATTR_NAME_SEP + title;
                if(doc.getTitle() == null){
                    doc.setTitle(titleAttrVal);
                }else{
                    String prevTitle = doc.getTitle();
                    titleAttrVal = prevTitle + DCIPreprocessorConstants.VALUE_SEP + titleAttrVal;
                    doc.setTitle(titleAttrVal);
                }
            }else if((xmlEvent.isEndElement())&&(xmlEvent.asEndElement().getName().getLocalPart().equals(DCIPreprocessorConstants.STR))){
                break;
            }
        }
    }
}
