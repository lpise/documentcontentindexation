package com.schneider.preprocessor;

import com.schneider.preprocessor.model.DCIDocument;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by SESA380090 on 2016-06-17.
 */
public class DCIXMLWriter {

    private static final Logger LOG = Logger.getLogger("[DCIXMLWriter]");
    private static String noLangTarget = DCIPreprocessorConstants.BLANK;

    static HashMap<String, String> messageDigestMap = new HashMap<String, String>();
    static HashMap<String, String> messageDigestMapFromStatusFile = null;


    /**
     * Name: writeXML
     * Description: This method the list of DCIDocument to the
     * destination XML file
     * Algorithm:
     * 1) Create the full path for destination XML file.
     * 2) Manage destination path using manageDestinationDirectory method.
     *            3) Write the XML header.
     *            4) Write <result>
     * 5) Fetch the various elements for a <doc> tag from corresponding DCIDocument.
     * 6) In case a document documents contains multiple range id then split the value
     * of field rangeId and write the details as separate <doc> elements for
     * each range id from the split array. Also log a message for the
     * highlighting the presence of multiple range ids.
     *            7) Write </result>
     *
     * @param xmlDestFolderPath
     * @param xmlFileName
     * @param docList
     * @throws java.io.IOException
     * @throws javax.xml.stream.XMLStreamException
     */
    public static void writeXML(String xmlDestFolderPath, List<DCIDocument> docList, HashMap<String, String[]> preprocessorProps, HashMap<String, List> docTypeMap)throws IOException, XMLStreamException {
        int addCount = 0;
        int updateCount = 0;

        String defaultLang = preprocessorProps.get(DCIPreprocessorConstants.DEFAULT_LANG)[0];

        //Find the complete path of the root destination folder
        File xmlDestFolder = new File(xmlDestFolderPath);
        String folderPath = xmlDestFolder.getAbsolutePath();

        //Find the number of DCIDocument objects in list
        int numDocs = docList.size();
        //Loop over the list to process DCIDocument
        for (int docIndex = 0; docIndex < numDocs; docIndex++) {
            //Take a DCIDocument on the list
            DCIDocument doc = docList.get(docIndex);

            LOG.info("DOC ID - "+doc.getId());

            //Create an array of rangeIds of DCIDocument
            String[] rangeIds = convertToArray(doc.getRangeId());
            int numRangeId = rangeIds.length;

            //Create an array of targets of DCIDocument
            String[] targets = convertToArray(doc.getTarget());
            int numTargets = targets.length;

            for(int rngIndex=0; rngIndex<numRangeId; rngIndex++){
                ArrayList<String> uniqueTargetList = new ArrayList<String>();
                String range = rangeIds[rngIndex];
                for(int cntryIndex=0; cntryIndex<numTargets; cntryIndex++){
                    String target = targets[cntryIndex];
                    if(!target.split(DCIPreprocessorConstants.COLON)[0].equals(DCIPreprocessorConstants.INVALID_CNTRY)){
                        target = target.substring(1, 3).toUpperCase();
                        if(!uniqueTargetList.contains(target)){
                            uniqueTargetList.add(target);
                            String[] langArr = preprocessorProps.get(target);
                            if(langArr!=null){
                                for(int langIndex=0; langIndex<langArr.length; langIndex++){
                                    String targetLang = langArr[langIndex];

                                    String targetLangForPath = "";
                                    if(targetLang.contains(DCIPreprocessorConstants.UNDERSCORE)){
                                        targetLangForPath = targetLang.split(DCIPreprocessorConstants.UNDERSCORE)[0];
                                    }else{
                                        LOG.info("Improper language " + targetLang + " for target " + target);
                                        continue;
                                    }

                                    //Find the message digest - create the file content
                                    byte[] docContentBytes = getDocContentBytes(doc, range, target, targetLang, defaultLang);
                                    String docContentMessageDigest = getMessageDigest(docContentBytes);

                                    String cntryLangXmlFolderPath = folderPath + System.getProperty("file.separator") + target + System.getProperty("file.separator") + targetLangForPath + System.getProperty("file.separator");
                                    String filePath = cntryLangXmlFolderPath + doc.getId() + DCIPreprocessorConstants.UNDERSCORE + range + DCIPreprocessorConstants.XML_EXTENSION;

                                    File cntryLangXmlFolder = new File(cntryLangXmlFolderPath);

                                    if(!cntryLangXmlFolder.exists()){
                                        cntryLangXmlFolder.mkdirs();
                                    }

                                    File destXmlFile = new File(filePath);
                                    String messageDigestMapKey = target + DCIPreprocessorConstants.UNDERSCORE + targetLangForPath + DCIPreprocessorConstants.UNDERSCORE + doc.getId() + DCIPreprocessorConstants.UNDERSCORE + range;


                                    if((messageDigestMapFromStatusFile!=null)&&(messageDigestMapFromStatusFile.containsKey(messageDigestMapKey))&&(!messageDigestMap.containsKey(messageDigestMapKey))){
                                        if((messageDigestMapFromStatusFile.get(messageDigestMapKey).equals(docContentMessageDigest))){
                                            messageDigestMapFromStatusFile.remove(messageDigestMapKey);
                                            messageDigestMap.put(messageDigestMapKey, docContentMessageDigest);
                                        }else{
                                            createXMLFile(destXmlFile, docContentBytes);
                                            messageDigestMapFromStatusFile.remove(messageDigestMapKey);
                                            messageDigestMap.put(messageDigestMapKey, docContentMessageDigest);
                                            updateCount = updateCount + 1;
                                        }
                                    }else{
                                        if(!messageDigestMap.containsKey(messageDigestMapKey)){
                                            createXMLFile(destXmlFile, docContentBytes);
                                            messageDigestMap.put(messageDigestMapKey, docContentMessageDigest);
                                            addCount = addCount + 1;
                                        }
                                    }
                                }
                            } else {
                                noLangTarget = manageNoInfoTarget(noLangTarget, target);
                            }

                        }

                    }
                }
            }
        }
        //Delete unreachable documents
        deleteFiles(messageDigestMapFromStatusFile, folderPath);

        if(noLangTarget.length()>0){
            LOG.info("[CONFIG WARNING] No target language(s) configured for targets " + noLangTarget);
        }
    }



    /**
     * This method creates a byte array for each <doc>
     * @param doc
     * @param rangeId
     * @param target
     * @param language
     * @return
     * @throws XMLStreamException
     */
    public static byte[] getDocContentBytes(DCIDocument doc, String rangeId, String target, String language, String defaultLang) throws XMLStreamException{

        String id = doc.getId();
        String comRef = doc.getCommercialRef();
        String targetFromDoc = doc.getTarget();
        String desc = doc.getDescription();
        String longDesc = doc.getLongDescription();
        String shortDesc = doc.getShortDescription();
        String origin = doc.getOrigin();
        String image = doc.getImage();

        //WMS-151
        String localEAN = doc.getLocalEanCode();
        String localDistCode = doc.getLocalDistCode();
        LOG.debug(language+" WMS 151 localEAN : "+localEAN);

        String edmsName = doc.getEdmsName();
        String title = doc.getTitle();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
        XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(byteArrayOutputStream, DCIPreprocessorConstants.ENCODING);

        //Write document header
        xmlStreamWriter.writeStartDocument(DCIPreprocessorConstants.ENCODING, DCIPreprocessorConstants.VERSION);

        String[] targets = null;

        //doc
        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.DOC);

        //attr_targets
        if (targetFromDoc.contains(DCIPreprocessorConstants.VALUE_SEP)) {
            targets = targetFromDoc.split(DCIPreprocessorConstants.VALUE_SEP);
        }else{
            targets = new String[1];
            targets[0] = targetFromDoc;
        }

        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.ARR);
        xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, DCIPreprocessorConstants.TARGETS);
        for(int targetIndex=0; targetIndex<targets.length; targetIndex++){
            if (targets[targetIndex].contains("\"" + target + "\"")) {
                xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
                xmlStreamWriter.writeCharacters(targets[targetIndex]);
                xmlStreamWriter.writeEndElement();
            }
        }
        xmlStreamWriter.writeEndElement();

        //commercialRef_s
        //writeValues(DCIPreprocessorConstants.COMM_REF, comRef, xmlStreamWriter);

        //id
        writeValues(DCIPreprocessorConstants.ID, id, xmlStreamWriter);

        //EDMS Name
        writeValues(DCIPreprocessorConstants.NAME, edmsName, xmlStreamWriter);

        //origin_s
        if(origin!=null){
            writeValues(DCIPreprocessorConstants.ORIGIN, doc.getOrigin(), xmlStreamWriter);
        }

        // productImageDocOid_s
        if (image!= null) {
            writeValues(DCIPreprocessorConstants.IMG, doc.getImage(), xmlStreamWriter);
        }

        //attr_RangeId
        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.ARR);
        xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, DCIPreprocessorConstants.RANGEID);
        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
        xmlStreamWriter.writeCharacters(rangeId);
        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndElement();

        //Fetch the country language from language configured for a country and create tags for longDescrition, shortDescription and Description
        String[] langArr = language.split(DCIPreprocessorConstants.UNDERSCORE);
        String countryLang = DCIPreprocessorConstants.LANG_SEP + langArr[0] + DCIPreprocessorConstants.CNTRY_SEP + langArr[1];
        String longDescTag = countryLang + DCIPreprocessorConstants.LONG_DESC;
        String shortDescTag = countryLang + DCIPreprocessorConstants.SHORT_DESC;
        String longTitleTag = countryLang + DCIPreprocessorConstants.TITLE;
        String descTag = countryLang + DCIPreprocessorConstants.DESCRIPTION;
        //WMS-151
        String eanCodeTag = "CO" + langArr[1] + "_country" + DCIPreprocessorConstants.EANCODE;
        String distCodeTag = "CO" + langArr[1] + DCIPreprocessorConstants.DISTRIB_CODE;
        LOG.debug(" longTitleTag : "+longTitleTag);

        //Fetch the country language from language configured as default and create tags for longDescrition, shortDescription and Description
        String[] defaultLangArr = defaultLang.split(DCIPreprocessorConstants.UNDERSCORE);
        String defaultCountryLang = DCIPreprocessorConstants.LANG_SEP + defaultLangArr[0] + DCIPreprocessorConstants.CNTRY_SEP + defaultLangArr[1];
        String defaultLongDescTag = defaultCountryLang + DCIPreprocessorConstants.LONG_DESC;
        String defaultShortDescTag = defaultCountryLang + DCIPreprocessorConstants.SHORT_DESC;
        String defaultDescTag = defaultCountryLang + DCIPreprocessorConstants.DESC;

        //Fetch country language based descriptions and write those if they are available
        String cntryLangLongDesc = DCIPreprocessorConstants.BLANK;
        String cntryLangShortDesc = DCIPreprocessorConstants.BLANK;
        String cntryLangDesc = DCIPreprocessorConstants.BLANK;

        //WMS-151
        String cntryLangEANCode = DCIPreprocessorConstants.BLANK;
        String cntryLangDISTCode = DCIPreprocessorConstants.BLANK;

        if ((longDesc!= null)&&(longDesc.contains(longDescTag))) {
            cntryLangLongDesc = fetchDescription(longDesc, longDescTag);
            writeDescriptions(cntryLangLongDesc, longDescTag, xmlStreamWriter);
        }
        if ((shortDesc!= null)&&(shortDesc.contains(shortDescTag))) {
            cntryLangShortDesc = fetchDescription(shortDesc, shortDescTag);
            writeDescriptions(cntryLangShortDesc, shortDescTag, xmlStreamWriter);
        }
        if((desc!=null)&&(desc.contains(descTag))){
            cntryLangDesc = fetchDescription(desc, descTag);
            LOG.info(cntryLangDesc+" < cntryLangDesc");
            writeDescriptions(cntryLangDesc, descTag, xmlStreamWriter);
        }
        if((localEAN!=null)&&(localEAN.contains(eanCodeTag))){
            cntryLangEANCode = fetchDescription(localEAN, eanCodeTag);
            writeDescriptions(cntryLangEANCode, eanCodeTag, xmlStreamWriter);
        }
        if((localDistCode!=null)&&(localDistCode.contains(distCodeTag))){
            cntryLangDISTCode = fetchDescription(localDistCode, distCodeTag);
            writeDescriptions(cntryLangDISTCode, distCodeTag, xmlStreamWriter);
        }
        if ((title!= null)&&(title.contains(longTitleTag))) {
            cntryLangShortDesc = fetchDescription(title, longTitleTag);
            writeDescriptions(cntryLangShortDesc, longTitleTag, xmlStreamWriter);
        }


        //If all the three descriptions are blank for country language then write default country language descriptions
        if((cntryLangLongDesc.equals(DCIPreprocessorConstants.BLANK))&&(cntryLangShortDesc.equals(DCIPreprocessorConstants.BLANK))&&(cntryLangDesc.equals(DCIPreprocessorConstants.BLANK))){
            if((longDesc!=null)&&(longDesc.contains(defaultLongDescTag))){
                String defaultLongDesc = fetchDescription(longDesc, defaultLongDescTag);
                writeDescriptions(defaultLongDesc, longDescTag, xmlStreamWriter);
            }
            if((shortDesc!=null)&&(shortDesc.contains(defaultShortDescTag))){
                String defaultShortDesc = fetchDescription(shortDesc, defaultShortDescTag);
                writeDescriptions(defaultShortDesc, shortDescTag, xmlStreamWriter);
            }
            if((desc!=null)&&(desc.contains(defaultDescTag))){
                String defaultDesc = fetchDescription(desc, defaultDescTag);
                writeDescriptions(defaultDesc, descTag, xmlStreamWriter);
            }
        }

        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();

        xmlStreamWriter.flush();
        xmlStreamWriter.close();

        return byteArrayOutputStream.toByteArray();
    }


    /**
     * This method is no more used since release 1.4.25.1
     * This method writes a single <doc> tag details
     *
     * @param xmlStreamWriter
     * @param target
     * @param longDescription
     * @param shortDescription
     * @param description
     * @param rangeId
     * @param commercialRef
     * @param id
     * @param origin
     * @throws XMLStreamException
     * @throws IOException
     */
//	static void writeDocument(DCIDocument doc, String rangeId, String target, String language, String folderPath) throws XMLStreamException,IOException {
//		String id = doc.getId();
//		String comRef = doc.getCommercialRef();
//		String targetFromDoc = doc.getTarget();
//		String desc = doc.getDescription();
//		String longDesc = doc.getLongDescription();
//		String shortDesc = doc.getShortDescription();
//		String origin = doc.getOrigin();
//
//		String xmlDestPath = folderPath + System.getProperty("file.separator") + target + System.getProperty("file.separator") + language + System.getProperty("file.separator");
//		File xmlDestFolder = new File(xmlDestPath);
//
//		if(!xmlDestFolder.exists()){
//			xmlDestFolder.mkdirs();
//		}
//
//		xmlDestPath = xmlDestPath + id + DCIPreprocessorConstants.UNDERSCORE + rangeId + DCIPreprocessorConstants.XML_EXTENSION;
//		FileOutputStream fileOutputStream = new FileOutputStream(xmlDestPath);
//		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
//		XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(fileOutputStream, DCIPreprocessorConstants.ENCODING);
//		xmlStreamWriter.writeStartDocument(DCIPreprocessorConstants.ENCODING, DCIPreprocessorConstants.VERSION);
//
//		String[] targets = null;
//
//		// doc
//		xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.DOC);
//
//		// attr_targets
//		if (targetFromDoc.contains(DCIPreprocessorConstants.VALUE_SEP)) {
//			targets = targetFromDoc.split(DCIPreprocessorConstants.VALUE_SEP);
//		} else {
//			targets = new String[1];
//			targets[0] = targetFromDoc;
//		}
//
//		xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.ARR);
//		xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, DCIPreprocessorConstants.TARGETS);
//		for (int targetIndex = 0; targetIndex < targets.length; targetIndex++) {
//			if (targets[targetIndex].contains("\"" + target + "\"")) {
//				xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
//				xmlStreamWriter.writeCharacters(targets[targetIndex]);
//				xmlStreamWriter.writeEndElement();
//			}
//		}
//		xmlStreamWriter.writeEndElement();
//
//		// commercialRef_s
//		writeValues(DCIPreprocessorConstants.COMM_REF, comRef, xmlStreamWriter);
//
//		// id
//		writeValues(DCIPreprocessorConstants.ID, id, xmlStreamWriter);
//
//		// origin_s
//		if (origin!= null) {
//			writeValues(DCIPreprocessorConstants.ORIGIN, doc.getOrigin(), xmlStreamWriter);
//		}
//
//		// attr_RangeId
//		xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.ARR);
//		xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME,DCIPreprocessorConstants.RANGEID);
//		xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
//		xmlStreamWriter.writeCharacters(rangeId);
//		xmlStreamWriter.writeEndElement();
//		xmlStreamWriter.writeEndElement();
//
//		//countryLang
//		String countryLang = DCIPreprocessorConstants.LANG_SEP + language + DCIPreprocessorConstants.CNTRY_SEP + target;
//		String longDescTag = countryLang + DCIPreprocessorConstants.LONG_DESC;
//		String shortDescTag = countryLang + DCIPreprocessorConstants.SHORT_DESC;
//		String descTag = countryLang + DCIPreprocessorConstants.DESC;
//
//		// longDesc_t
//		if ((longDesc!= null)&&(longDesc.contains(longDescTag))) {
//			writeDescriptions(doc.getLongDescription(), xmlStreamWriter, longDescTag);
//		}
//
//		// shortDesc_t
//		if ((shortDesc!= null)&&(shortDesc.contains(shortDescTag))) {
//			writeDescriptions(doc.getShortDescription(), xmlStreamWriter, shortDescTag);
//		}
//
//		// desc_t
//		if (((desc!= null))&&(doc.getDescription().contains(descTag))) {
//			writeDescriptions(doc.getDescription(), xmlStreamWriter, descTag);
//		}
//
//		xmlStreamWriter.writeEndElement();
//		xmlStreamWriter.writeEndDocument();
//
//		xmlStreamWriter.flush();
//		xmlStreamWriter.close();
//
//		fileOutputStream.flush();
//		fileOutputStream.close();
//	}

    /**
     * This method directly writes the values
     *
     * @param attrName
     * @param value
     * @param xmlStreamWriter
     * @throws XMLStreamException
     */
    public static void writeValues(String attrName, String value, XMLStreamWriter xmlStreamWriter) throws XMLStreamException{
        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
        xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, attrName);
        xmlStreamWriter.writeCharacters(value);
        xmlStreamWriter.writeEndElement();

    }


    /**
     * Fetch descriptions
     */
    public static String  fetchDescription(String description, String tag){

        LOG.debug(description+" -- WMS 151 Fetch Local Dist Code : "+tag);
        if(description.contains(DCIPreprocessorConstants.VALUE_SEP)){
            int beginIndex = description.indexOf(tag);
            int endIndex = description.indexOf(DCIPreprocessorConstants.VALUE_SEP, beginIndex);
            if((beginIndex!=-1)&&(endIndex!=-1)){
                description = description.substring(beginIndex, endIndex);
            }else if(beginIndex!=-1){
                description = description.substring(beginIndex);
            }
        }
        return description.split(DCIPreprocessorConstants.ATTR_NAME_SEP)[1];
    }

    /**
     * This method is used to write description elements
     *
     * @param description
     * @param xmlStreamWriter
     * @throws XMLStreamException
     */
//	public static void writeDescriptions(String description, XMLStreamWriter xmlStreamWriter, String tag)throws XMLStreamException {
//		if (description.contains(DCIPreprocessorConstants.VALUE_SEP)) {
//			int beginIndex = description.indexOf(tag);
//			int endIndex = description.indexOf(DCIPreprocessorConstants.VALUE_SEP, beginIndex);
//			if((beginIndex!=-1)&&(endIndex!=-1)){
//			description = description.substring(beginIndex, endIndex);
//			}else if(beginIndex!=-1){
//				description = description.substring(beginIndex);
//			}
//		}
//		    String[] descParts = description.split(DCIPreprocessorConstants.ATTR_NAME_SEP);
//			xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
//			xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, descParts[0]);
//			xmlStreamWriter.writeCharacters(descParts[1]);
//			xmlStreamWriter.writeEndElement();
//
//	}

    /**
     *
     */
    public static void writeDescriptions(String desc, String descTag, XMLStreamWriter xmlStreamWriter) throws XMLStreamException {
        xmlStreamWriter.writeStartElement(DCIPreprocessorConstants.STR);
        xmlStreamWriter.writeAttribute(DCIPreprocessorConstants.NAME, descTag);
        xmlStreamWriter.writeCharacters(desc);
        xmlStreamWriter.writeEndElement();
    }

    /**
     * This method converts a multi valued string into an array
     *
     * @param docInfo
     * @return docInfoArray
     */
    public static String[] convertToArray(String docInfo){
        String[] docInfoArray = null;
        if(docInfo.contains(DCIPreprocessorConstants.VALUE_SEP)){
            docInfoArray = docInfo.split(DCIPreprocessorConstants.VALUE_SEP);
        }else{
            docInfoArray = new String[1];
            docInfoArray[0] = docInfo;
        }
        return docInfoArray;
    }

    /**
     * This method manages the destination folder
     *
     * @param xmlDestFolder
     * @throws IOException
     */
    public static void manageDestinationDirectory(File xmlDestFolder) throws IOException{
        if (!xmlDestFolder.exists()) {
            LOG.info("Creating new destination folder...");
            xmlDestFolder.mkdirs();
        } else {
            LOG.info("Deleting previous destination folder...");
            FileUtils.deleteDirectory(xmlDestFolder);
            LOG.info("Creating new destination folder...");
            xmlDestFolder.mkdirs();
        }
    }

    /**
     * This method consolidates information for logging
     *
     * @param noInfoTarget
     * @param target
     * @return
     */
    public static String manageNoInfoTarget(String noInfoTarget, String target){
        if(noInfoTarget.equals(DCIPreprocessorConstants.BLANK)){
            noInfoTarget = noInfoTarget + target;
        }else if(!noInfoTarget.contains(target)){
            noInfoTarget = noInfoTarget + DCIPreprocessorConstants.COMMA + target;
        }
        return noInfoTarget;
    }

    /**
     * This method creates message digest for a <doc> content
     * @param messageBuffer
     * @return
     */
    public static String getMessageDigest(byte[] messageBuffer){
        StringBuffer mdStrBuffer = new StringBuffer();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(messageBuffer);
            byte[] digest = md.digest();
            //convert the byte to hex format method 1
            for (int i = 0; i < digest.length; i++) {
                mdStrBuffer.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) {
            LOG.debug("Exception while creating message digest", e);
        }
        return mdStrBuffer.toString();
    }

    /**
     * This method writes output file
     * @param destXmlFile
     * @param docContentBytes
     * @throws IOException
     */
    public static void createXMLFile(File destXmlFile, byte[] docContentBytes) throws IOException{
        if(destXmlFile.exists()){
            destXmlFile.delete();
        }
        destXmlFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(destXmlFile);
        fos.write(docContentBytes);
        fos.close();
    }

    /**
     * This method reads message digests from status file
     * @param statusFile
     * @return
     * @throws IOException
     */
    public static HashMap<String, String> getMessageDigestsFromStatusFile(File statusFile) throws IOException{
        HashMap<String, String> messageDigestMap = new HashMap<String, String>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(statusFile));
        String messageDigestInfo = "";
        while((messageDigestInfo=bufferedReader.readLine())!=null){
            String path = messageDigestInfo.split(DCIPreprocessorConstants.VALUE_SEP)[0];
            String md = messageDigestInfo.split(DCIPreprocessorConstants.VALUE_SEP)[1];
            messageDigestMap.put(path, md);
        }
        bufferedReader.close();
        return messageDigestMap;
    }

    /**
     * This method writes status file
     * @param statusFile
     * @param messageDigestMap
     * @throws IOException
     */
    public static void writeStatusFile(File statusFile, HashMap<String, String> messageDigestMap) throws IOException{
        if(statusFile.exists()){
            statusFile.delete();
        }
        statusFile.createNewFile();
        LOG.info("Updating status file...");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(statusFile, true));
        Set<String> keySet = messageDigestMap.keySet();
        for(String key : keySet){
            bufferedWriter.write(key + DCIPreprocessorConstants.VALUE_SEP + (String)messageDigestMap.get(key));
            bufferedWriter.write("\n");
        }
        bufferedWriter.close();
    }

    /**
     * This method deletes non existing files
     * @param messageDigestMapFromStatusFile
     */
    private static void deleteFiles(HashMap<String, String> messageDigestMapFromStatusFile, String destFolderPath) {
        if(messageDigestMapFromStatusFile!=null){
            LOG.info("Deleting unreachable files...");
            Set<String> keySet = messageDigestMapFromStatusFile.keySet();
            for(String key : keySet){
                String[] keyArr = key.split(DCIPreprocessorConstants.UNDERSCORE);
                StringBuffer filePath  = new StringBuffer(destFolderPath);
                filePath.append(System.getProperty("file.separator"));
                filePath.append(keyArr[0]);
                filePath.append(System.getProperty("file.separator"));
                filePath.append(keyArr[1]);
                filePath.append(System.getProperty("file.separator"));
                filePath.append(keyArr[2]);
                filePath.append(DCIPreprocessorConstants.UNDERSCORE);
                filePath.append(keyArr[3]);
                filePath.append(DCIPreprocessorConstants.XML_EXTENSION);
                File file = new File(filePath.toString());
                if(file.exists()){
                    file.delete();
                }
            }
            LOG.info("Number of files deleted: " + keySet.size());
        }

    }
}
