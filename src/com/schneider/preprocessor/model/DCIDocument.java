package com.schneider.preprocessor.model;

/**
 * Created by SESA380090 on 2016-06-15.
 */
public class DCIDocument {

    private String target;
    private String longDescription;
    private String shortDescription;
    private String description;
    private String rangeId;
    private String commercialRef;
    private String id;
    private String origin;
    private String image;

    //WMS-151
    private String localEanCode;
    private String localDistCode;
    private String highlightedChar;
    private String localPartNumber;

    private String docTypeID;
    private String title;
    private String edmsName;

    public String getTarget() {
        return target;
    }
    public void setTarget(String target) {
        this.target = target;
    }
    public String getLongDescription() {
        return longDescription;
    }
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    public String getShortDescription() {
        return shortDescription;
    }
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getRangeId() {
        return rangeId;
    }
    public void setRangeId(String rangeId) {
        this.rangeId = rangeId;
    }
    public String getCommercialRef() {
        return commercialRef;
    }
    public void setCommercialRef(String commercialRef) {
        this.commercialRef = commercialRef;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getOrigin() {
        return origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    //WMS-151
    public String getLocalDistCode() {
        return localDistCode;
    }
    public void setLocalDistCode(String localDistCode) {
        this.localDistCode = localDistCode;
    }
    public String getLocalEanCode() {
        return localEanCode;
    }
    public void setLocalEanCode(String localEanCode) {
        this.localEanCode = localEanCode;
    }
    public String getHighlightedChar() {
        return highlightedChar;
    }
    public void setHighlightedChar(String highlightedChar) {
        this.highlightedChar = highlightedChar;
    }
    public String getLocalPartNumber() {
        return localPartNumber;
    }
    public void setLocalPartNumber(String localPartNumber) {
        this.localPartNumber = localPartNumber;
    }

    public String getDocTypeID() {
        return docTypeID;
    }
    public void setDocTypeID(String docTypeID) {
        this.docTypeID = docTypeID;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getEdmsName() {
        return edmsName;
    }
    public void setEdmsName(String edmsName) {
        this.edmsName = edmsName;
    }

    @Override
    public String toString() {
        return "OnePOPDocument [target=" + target
                + ", longDescription=" + longDescription
                + ", shortDescription=" + shortDescription
                + ", description=" + description
                + ", rangeId=" + rangeId + ", commercialRef=" + commercialRef
                + ", id=" + id + ", origin=" + origin + ", image =" + image
                + ", localDistCode=" + localDistCode + ", localEanCode=" + localEanCode
                + ", highlightedChar =" + highlightedChar +", localPartNumber =" + localPartNumber+"]";
    }
}
