package com.schneider.preprocessor;

/**
 * Created by SESA380090 on 2016-06-15.
 */
public class DCIPreprocessorConstants {

    public static final String ENCODING = "UTF-8";
    public static final String VERSION = "1.0";
    public static final String RESULT = "result";
    public static final String DOC = "doc";
    public static final String STR = "str";
    public static final String ARR = "arr";
    public static final String TARGETS = "attr_targets";
    public static final String LONG_DESC = "_longDesc_t";
    public static final String SHORT_DESC = "_shortDesc_t";
    public static final String DESC = "_desc_t";
    //public static final String RANGEID = "attr_RangeId";
    public static final String RANGEID = "attr_rangeId";
    public static final String COMM_REF = "commercialRef_s";
    public static final String ID = "id";
    public static final String ORIGIN = "origin_s";
    public static final String PRD_ID = "productId_s";
    public static final String IMG = "productImageDocOid_s";
    public static final String NAME = "name";
    public static final String ATTR_NAME_SEP = "-!-";
    public static final String VALUE_SEP = "-#-";
    public static final String BLANK = "";
    public static final String LANG_SEP = "LL";
    public static final String CNTRY_SEP = "_LC";
    public static final String COLON = ";";
    public static final String COMMA = ",";
    public static final String UNDERSCORE = "_";
    public static final String XML_EXTENSION = ".xml";
    public static final String INVALID_CNTRY = "\"\"";
    public static final String DEFAULT_LANG = "DEFAULT_LANG";

    //New stuff for WMS-151
    public static final String EANCODE = "EanCode_s";
    public static final String DISTRIB_CODE = "_localDistCode_s";
    public static final String HIGHLIGHTED_CHAR = "_highlightedProductChar";
    public static final String NAME_T = "_name_t";
    public static final String VALUE_T = "_value_t";
    public static final String LOCAL_PARTNUMBER = "_localPartNumber_s";
    public static final String DOCS_FOR_AUTONOM = "DOCS_FOR_AUTONOM";
    public static final String FILENAME = "attr_fileName";
    public static final String TITLE = "_Title_t";
    public static final String DESCRIPTION = "_Description_t";
    public static final String DOC_TYPE_ID = "docTypeOid_s";
    public static final String DOC_TYPE_SERVICE = "DOC_TYPE_SERVICE";
}
